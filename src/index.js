import React from 'react'
import ReactDOM from 'react-dom'

import { ListImage } from './components/ListImage'

ReactDOM.render(
    <ListImage />,
    document.getElementById('app')
)