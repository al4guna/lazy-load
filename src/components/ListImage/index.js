import React  from 'react'

import { List } from './styled'

import { Image } from '../Image/'

const data = [
    { src : 'https://cdn.pixabay.com/photo/2017/08/06/03/07/people-2588172_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2016/11/14/02/33/adult-1822413_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2015/02/02/11/09/office-620822_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2018/01/09/00/49/people-3070630_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2017/06/13/09/32/beauty-2398183_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2016/04/26/16/57/pills-1354782_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2017/04/03/11/38/woman-2198083_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2019/09/28/18/28/bed-4511477_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2017/08/18/13/07/sea-2654890_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2016/11/14/03/35/bed-1822497_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2019/12/15/08/16/nude-4696548_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2014/01/20/08/33/ladybug-248481_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2018/01/09/00/49/people-3070630_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2016/11/08/05/26/woman-1807533_960_720.jpg' },    { src : 'https://cdn.pixabay.com/photo/2017/08/18/13/07/sea-2654890_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2015/07/09/00/29/woman-837156_960_720.jpg' },
    { src : 'https://cdn.pixabay.com/photo/2019/12/15/08/16/nude-4696548_960_720.jpg' },
]

export const ListImage = () => {
    return(
        <List>
            { data.map( (elem, key ) => <Image key={key} {...elem}/> ) }
        </List>
    )
}