import styled from 'styled-components'

export const Article = styled.article`
    min-height: 230px;
    overflow: hidden;
    border-radius: 5px;
    margin: 5px;
`

export const Img = styled.img`
    width: 100%;
    height: auto;
    border-radius: 5px;
    animation: 1s in ease;
    @keyframes in {
        from {
          margin-left:100%;
          width:300%
        }
        
        to {
         margin-left:0%;
         width:100%;
       }
    }
`

