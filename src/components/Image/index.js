import React, { Fragment } from 'react'

import { useNearScreen } from '../../hocks/useNearScreen'

import { Article, Img } from './styled'

export const Image = ({ src }) => {

    const [ show, ref ] = useNearScreen()
    return(
        <Article ref={ref}>
            { show &&
                <Fragment>
                    <Img src={src}/>
                </Fragment>
            }
        </Article>
    )
}